//Soal 1
function halo(){
    a="Halo Sanbers!";
    return a;
  }

console.log(halo()) 


//Soal 2
function kalikan(num1,num2) {
    var c=(num1*num2);
    return c;
}
var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log(kalikan) // 48


//Soal 3
function introduce(name,age,address,hobby) {
    return "Nama saya "+name+" umur saya "+age+" tahun, alamat saya "+address+" dan saya punya hobby yaitu "+hobby+"!";
}
 var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) 


//Soal 4
var arrayDaftarPeserta = ['Asep', 'laki-laki', 'baca buku' , '1992']
var dataObj = {nama: arrayDaftarPeserta[0],
            jenis_kelamin: arrayDaftarPeserta[1],
            hobby: arrayDaftarPeserta[2],
            tahun_lahir: arrayDaftarPeserta[3]}

console.log(dataObj)


//Soal 5
var data5=[{nama: 'strawberry',
  warna: 'merah',
  ada_bijinya: 'tidak',
  harga: '9000' },
{nama: 'jeruk',
  warna: 'oranye',
  ada_bijinya: 'ada',
  harga: '8000'},
{nama: 'Semangka',
  warna: 'Hijau & Merah',
  ada_bijinya: 'ada',
  harga: '10000'},
{nama: 'Pisang',
  warna: 'Kuning',
  ada_bijinya: 'tidak',
  harga: '5000'}]
 
  console.log(data5[0])

//Soal 6
var dataFilm = []

function tambahFilm(film1,durasi1,genre1,tahun1) {
    dataFilm.push({judul:film1, durasi:durasi1, genre:genre1, tahun:tahun1})
    console.log()

}
tambahFilm("Thor","120 menit","fantasy","2010")
tambahFilm("Avengers","120 menit","action","2018")
console.log(dataFilm)
