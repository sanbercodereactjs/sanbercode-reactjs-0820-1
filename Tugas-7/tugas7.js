//Soal 1
console.log('---Soal 1---')
//release 0
class Animal {
    constructor(name){
        this.nama=name
        this._legs=4
        this._cold_blooded=false
    }
    get name(){
        return this.nama
    }
    set name(name){
        this.nama=name
    }

    get legs(){
        return this._legs
    }
    set legs(legs){
        this._legs = legs
    }

    get cold_blooded(){
        return this._cold_blooded
    }
    set cold_blooded(cold_blooded){
        this._cold_blooded=cold_blooded
    }
}
console.log('release 0')
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log()

console.log('release 1')
class Ape extends Animal{
    constructor(name,legs){
        super(name);
        this._legs = legs
     }
     get name(){
         return this.nama
     }
     set name(name){
         this.nama=name
     }
     get legs(){
         return this._legs
     }
     set legs(legs){
         this._legs=legs
     }
     yell(){
         return "Auooo"
     }

}
class Frog extends Animal{
    constructor(name,legs){
        super(name,legs);
        this._legs = legs
     }
     get name(){
         return this.nama
     }
     set name(name){
         this.nama=name
     }
     get legs(){
         return this._legs
     }
     set legs(legs){
         this._legs=legs
     }
     jump(){
         return "hop hop"
     }

}

var sungokong = new Ape("kera sakti")
console.log(sungokong.name)
sungokong.legs=2
console.log(sungokong.legs)
sungokong.yell() // "Auooo"
console.log(sungokong.yell())
console.log
var kodok = new Frog("buduk")
console.log(kodok.name)
kodok.legs=4
console.log(kodok.legs)
kodok.jump() // "hop hop"
console.log(kodok.jump())
console.log()

console.log('---Soal 2---')
//Soal 2   
class Clock {
    constructor({template}){
    this.template=template;
    this.timer            
    }
    render(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    start(){
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }

    stop(){
        clearInterval(this.timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 