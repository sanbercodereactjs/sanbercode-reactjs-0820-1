//Soal 1
const KLingkaran = (phi,jarijari) => {
    return phi*(jarijari*2)
}

const LLingkaran = (phi,jarijari) =>{
    return phi*(jarijari*jarijari)
}
const phi = 3.147
let jarijari = 10
console.log('Luas lingkaran dengan jari-jari 10 adalah '+LLingkaran(phi,jarijari))
console.log('Keliling lingkaran dengan jari-jari 10 adalah '+KLingkaran(phi,jarijari))
console.log()


//Soal 2
let kalimat =""
const kata1='saya '
const kata2='adalah '
const kata3='seorang '
const kata4='fronted '
const kata5='developer '
const tambahkata = (kata1,kata2,kata3,kata4,kata5)=>{
    return kalimat=`${kalimat} ${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`   
}
console.log(tambahkata(kata1,kata2,kata3,kata4,kata5))
console.log()


//Soal 3
const newFunction=(firstName, lastName)=>
{
    return {
        firstName, lastName,
        fullName:()=>{
        console.log(`${firstName} ${lastName}`)
        return
        }
    }
 }
   
//Driver Code 
newFunction("William", "Imoh").fullName() 
console.log()


//Soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName,lastName,destination,occupation,spell} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)
console.log()


//Soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)