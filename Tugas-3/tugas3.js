// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama+" "+kataKedua[0].toUpperCase()+kataKedua.substr(1)+" "+kataKetiga+" "+kataKeempat.toUpperCase());


//soal 2
var kataPertama0 = "1";
var kataKedua0 = "2";
var kataKetiga0 = "4";
var kataKeempat0 = "5";

console.log(Number(kataPertama0)+Number(kataKedua0)+Number(kataKetiga0)+Number(kataKeempat0));


//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama1 = kalimat.substring(0, 3); 
var kataKedua1 = kalimat.substr(4,10); 
var kataKetiga1 = kalimat.slice(15,18); 
var kataKeempat1= kalimat.replace("wah javascript itu keren sekali","keren"); 
var kataKelima1= kalimat.match(/sekali/); 

console.log('Kata Pertama: ' + kataPertama1); 
console.log('Kata Kedua: ' + kataKedua1); 
console.log('Kata Ketiga: ' + kataKetiga1); 
console.log('Kata Keempat: ' + kataKeempat1); 
console.log('Kata Kelima: ' + kataKelima1);


//soal 4
var nilai = 45;
if (nilai >= 80 && nilai <=100) {
    console.log("A");
} else if (nilai >=70 && nilai < 80) {
        console.log("B");             
} else if (nilai >=60 && nilai <70) {
        console.log("c");        
} else if (nilai >=50 && nilai <60) {
        console.log("D");
} else if (nilai < 50) {
        console.log("E");} 
 else {
    console.log("nilai salah");
}


//soal 5
var tanggal = 21;
var bulan = 9;
var tahun = 1994;

switch(bulan){
 case 1: {bulan = 'Januari';break; }
 case 2: {bulan = 'February';break; }
 case 3: {bulan = 'Maret';break; }
 case 4: {bulan = 'April';break; }
 case 5: {bulan = 'Mei';break; }
 case 6: {bulan = 'Juni';break; }
 case 7: {bulan = 'Juli';break; }   
 case 8: {bulan = 'Agustus';break; }
 case 9: {bulan = 'September';break; }
 case 10: {bulan = 'Oktober';break; }
 case 11: {bulan = 'November';break; }
 case 12: {bulan = 'Desember';break; }
 default: {console.log('Tidak Ditemukan')}
}

console.log(tanggal +" "+ bulan + " " +tahun);
